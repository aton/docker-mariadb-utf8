MariaDB UTF8
============

Based on official [MariaDB image (latest)](https://hub.docker.com/_/mariadb/).

The only change is custom config file __my.cnf__ to set default collation and character set to **UTF-8** 
according to [official MariaDB documentation](https://mariadb.com/kb/en/mariadb/setting-character-sets-and-collations/).  

Usage
-----

See [official documentation](https://hub.docker.com/_/mariadb/).

Version
-------

- MariaDB version: 10.1
- Package version: 0.1.1

Maintainer: Alexander Pankov <pankov.ap@gmail.com>
